import { Component, OnInit, ViewChild } from '@angular/core';
import { PlaceholderService } from '../../services/placeholder.service';
import {Task} from '../../models/Task';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild('form') form;
  title: string;
  editTaskID: number;
  isEdit: boolean;

  constructor(
    public fakeserver: PlaceholderService
  ) { }

  ngOnInit() {
    this.fakeserver.editingTask.subscribe((task: Task) => {
      if(task.title) {
        this.isEdit = true;
        this.title = task.title;
        this.editTaskID = task.id;
        this.fakeserver.emitUpdateTask(task);
      }
    });
  }

  addTask() {
    const newTask = {
      userId: 1,
      completed: false,
      title: this.title,
    };

    this.fakeserver.addTask(newTask).subscribe((data:  Task) => {
      this.form.reset();
      this.fakeserver.emitNewTask(data)
    });
  }

  canselEditing(task) {
    this.form.reset();
    this.isEdit  = false;
  }

  editTask() {
    const updateTask = {
      id: this.editTaskID,
      title: this.title,
      userId: 1,
      completed: false
    };
    this.fakeserver.editTask(updateTask).subscribe(
      (task: Task) => {
        this.canselEditing(updateTask);
        this.fakeserver.emitUpdateTask(task);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
