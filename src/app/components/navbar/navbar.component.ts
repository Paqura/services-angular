import { Component, OnInit } from '@angular/core';
import { PlaceholderService } from '../../services/placeholder.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  todoLength: number;

  constructor(
    private fakeserver: PlaceholderService
  ) { }

  ngOnInit() {
    this.fakeserver.taskCount.subscribe((length: number) => {
      this.todoLength = length;
    });
  }
}
