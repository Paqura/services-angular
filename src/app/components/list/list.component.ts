import { Component, OnInit } from '@angular/core';
import { PlaceholderService } from '../../services/placeholder.service';
import { Task } from '../../models/Task';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  tasks: any;

  constructor(
    public fakeserver: PlaceholderService
  ) {}

  ngOnInit() {
    this.fakeserver.getTasks().subscribe(
      (data) => data && (this.tasks = data),
      (err) => console.log(err)
    );

    this.fakeserver.newTask.subscribe((data: Task) => {
      if(data['body']) {
        const newTask = Object.assign({}, data['body'], {id: data['id']});
        this.tasks.push(newTask);
        this.fakeserver.updateCount(this.tasks.length);
      }
    });


    this.fakeserver.editingTask.subscribe((task: Task) => {
      if(this.tasks) {
        this.tasks = this.tasks.map(item => {
          if(item.id === task.id) {
            item.isEdit = true;
          }
          return item;
        });
      }
    });

    this.fakeserver.updatingTask.subscribe((editedTask: Task) => {
      if(editedTask.title) {
        this.tasks = this.tasks.map(task => {
          if(task.id === editedTask.id) {
            task.title = editedTask.title;
            task.isEdit = false;
          }
          return task;
        });
      }
    });

  }

  identify(index) {
    return index;
  }

  completeTask(id) {
    this.fakeserver.completeTask(id).subscribe(data => {
      this.tasks = this.tasks.map(task => {
        if(task.id === id) {
          let isComplete = task.completed;
          task.completed = !isComplete;
        }
        return task;
      });
    });
  }

  deleteTask(id) {
    this.fakeserver.deleteTask(id).subscribe(data => {
      this.tasks = this.tasks.filter(task => task.id !== id);
      this.fakeserver.updateCount(this.tasks.length);
    });
  }

  editTask(task: Task) {
    this.fakeserver.emitEditTask(task);
  }
}
