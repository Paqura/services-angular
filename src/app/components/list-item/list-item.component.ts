import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../models/Task';
import { PlaceholderService } from '../../services/placeholder.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() task: Task;
  @Output() delete = new EventEmitter();
  @Output() complete = new EventEmitter();
  @Output() edit = new EventEmitter();

  constructor(
    public fakeserver: PlaceholderService
  ) { }

  ngOnInit() {
  }

  deleteTask() {
    this.delete.emit(this.task.id);
  }

  toggleComplete() {
    this.complete.emit(this.task.id);
  }

  editTask() {
    const updateTask = Object.assign({}, this.task);
    this.edit.emit(updateTask);
  }
}
