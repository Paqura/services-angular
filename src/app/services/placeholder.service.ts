import { Injectable } from '@angular/core';
import {Task} from '../models/Task';
import {HttpClient} from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {
  private endpoint: string = 'https://jsonplaceholder.typicode.com/todos/';
  private taskSource = new BehaviorSubject<Task>({id: 0, title: '', userId: 0, completed: false});
  private taskCountSource = new BehaviorSubject(200);
  private editTaskSource = new BehaviorSubject<Task>({id: 0, title: '', userId: 0, completed: false});
  private updateTaskSource = new BehaviorSubject<Task>({id: 0, title: '', userId: 0, completed: false});
  public newTask = this.taskSource.asObservable();
  public taskCount = this.taskCountSource.asObservable();
  public editingTask = this.editTaskSource.asObservable();
  public updatingTask = this.updateTaskSource.asObservable();

  constructor(
    public http: HttpClient
  ) {
  }

  emitNewTask(task: Task) {
    this.taskSource.next(task);
  }

  emitUpdateTask(task: Task) {
    return this.updateTaskSource.next(task);
  }

  updateCount(length: number) {
    this.taskCountSource.next(length);
  }

  getTasks() {
    return this.http.get(this.endpoint);
  }

  addTask(task: Task) {
    return this.http.post(this.endpoint, {
      body: task
    })
  }

  deleteTask(id: string) {
    return this.http.delete(this.endpoint + id);
  }

  completeTask(id: string) {
    return this.http.patch(this.endpoint + id, {
      body: id
    });
  }

  emitEditTask(task: Task) {
    this.editTaskSource.next(task);
  }

  editTask(task: Task) {
    return this.http.put(this.endpoint + task.id, {
      ...task
    });
  }
}
